use todo::task::Task;

fn main() {
    let first_todo = Task::new("First Todo!");
    let mut second_todo = Task::new("Second Todo!");
    second_todo.set_complete();
    println!("{}", first_todo);
    println!("{}", second_todo);
}

use wasm_bindgen::prelude::*;

use crate::task::Task;

#[wasm_bindgen]
#[derive(Debug)]
pub struct List {
    title: String,
    tasks: Vec<Task>,
    color: Option<(u8, u8, u8, u8)>,
}

impl List {
    pub fn new(title: &str) -> Self {
        Self {
            title: title.to_string(),
            tasks: Vec::new(),
            color: None,
        }
    }

    pub fn get_title(&self) -> String {
        self.title.to_string()
    }

    pub fn get_tasks(self) -> Vec<Task> {
        self.tasks
    }

    pub fn get_color(self) -> Option<(u8, u8, u8, u8)> {
        self.color
    }

    pub fn add_task(&mut self, task: Task) {
        self.tasks.push(task)
    }
}

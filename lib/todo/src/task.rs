// use time::OffsetDateTime;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[derive(Debug)]
pub struct Task {
    is_complete: bool,
    title: String,
    description: Option<String>,
    // due_date: Option<OffsetDateTime>,
}

#[wasm_bindgen]
impl Task {
    #[wasm_bindgen(constructor)]
    pub fn new(title: &str) -> Self {
        Self {
            is_complete: false,
            title: title.to_string(),
            description: None,
            // due_date: None,
        }
    }

    pub fn set_complete(&mut self) {
        self.is_complete = true
    }

    pub fn set_incomplete(&mut self) {
        self.is_complete = false
    }

    pub fn set_title(&mut self, title: &str) {
        self.title = title.to_string()
    }

    pub fn get_title(&self) -> String {
        self.title.to_string()
    }

    pub fn set_description(&mut self, desc: &str) {
        self.description = Some(desc.to_string())
    }

    // pub fn set_due_date(&mut self, date_time: OffsetDateTime) {
    //     self.due_date = Some(date_time)
    // }
}

impl std::fmt::Display for Task {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let status;
        if self.is_complete {
            status = String::from("[✔]")
        } else {
            status = String::from("[ ]");
        }
        write!(f, "{} {}", status, self.title)
    }
}
